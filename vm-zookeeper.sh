#!/usr/bin/env bash

apt-get update
apt-get install -y openjdk-8-jdk
cd /opt
wget -q https://archive.apache.org/dist/zookeeper/zookeeper-3.4.6/zookeeper-3.4.6.tar.gz
tar -xzf zookeeper-3.4.6.tar.gz

MYID=$1
mkdir -p /var/zookeeper/{data,conf}
echo -n $MYID > /var/zookeeper/data/myid
cat > /var/zookeeper/conf/zoo.cfg <<EOF
tickTime=2000
initLimit=10
syncLimit=5
dataDir=/var/zookeeper/data
clientPort=2181
server.1=192.168.12.11:2888:3888
maxClientCnxns=100
EOF

#cat > /etc/init/zookeeper.conf <<EOF
#exec /opt/zookeeper-3.4.6/bin/zkServer.sh start-foreground /var/zookeeper/conf/zoo.cfg
#EOF

cat > /etc/systemd/system/zookeeper.service <<EOF
# Modified from https://phoenixnap.com/kb/install-apache-zookeeper
[Unit]
Description=Zookeeper Daemon
Documentation=http://zookeeper.apache.org
Requires=network.target
After=network.target

[Service]
Type=forking
WorkingDirectory=/opt/zookeeper-3.4.6
User=zookeeper
Group=zookeeper
ExecStart=/opt/zookeeper-3.4.6/bin/zkServer.sh start /var/zookeeper/conf/zoo.cfg
ExecStop=/opt/zookeeper-3.4.6/bin/zkServer.sh stop /var/zookeeper/conf/zoo.cfg
ExecReload=/opt/zookeeper-3.4.6/bin/zkServer.sh restart /var/zookeeper/conf/zoo.cfg
TimeoutSec=30
Restart=on-failure

[Install]
WantedBy=default.target
EOF

#start zookeeper
#systemctl start zookeeper
/opt/zookeeper-3.4.6/bin/zkServer.sh start /var/zookeeper/conf/zoo.cfg
