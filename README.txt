* Install Vagrant, or if you prefer, Apache Zookeeper
* From a shell running with the Vagrantfile in the current directory, enter:
  vagrant up
* This will create a Linux virtual machine with Zookeeper installed, and will run a Zookeeper server. Alternatively just install Apache ZooKeeper manually without using a VM.
* NOTE THAT YOU WILL NEED TO RESTART ZOOKEEPER TO CLEAR ITS STATE BETWEEN EACH RUN OF THE AGENT APPLICATION. Use "vagrant ssh" to connect to the VM and enter "sudo stop zookeeper; sudo rm -f /var/zookeeper/data/version-2/*; sudo start zookeeper" (yes, a better way is needed!). Alternatively use the ZooKeeper command-line interface, zkCli (/opt/zookeeper-3.4.6/bin/zkCli.sh) in the VM to delete all ZooKeeper znodes: Enter "ls /" to see all top-level nodes, then use "rmr /<node>" (one at a time) to remove all of these except /zookeeper (which is for ZooKeeper's internal use). Then "quit" to exit.
* To run the agents (I use NetBeans on my host machine - not the VM), run otagoinfosci.bdigroupplans.App.java. This an Apache Camel router.
* The agent source code is in ...\src\main\resources\otagoinfosci\bdigroupplans. In NetBeans the resources folder is shown under "Other Sources". The file agents.ini specifies which agents are run.
* To see the print statements from the agents within the console output, filter the output for "INFO: ".
* Note that App.java runs continously. Terminate it to examine the output.
* Some org.apache.zookeeper.KeeperException$NodeExistsException messages appear in the output. These appear when routes try to create znodes that already exist. They are harmless (but I don't know how to avoid them).

Notes on the Camel routes in App.java:

The important part is the call to camel.addRoutes in the main method. This takes an argument that is an anonymous inner class with a configure method that defines a set of Camel routes. These routes have several purposes:

* There is a chain of routes that implement the master process – one ZooKeeper client is automatically elected to run these routes, which make decisions about the state of each active goal goal. (Note that currently there is only one ZooKeeper client, which runs all the agents; however, it is possible to run multiple instances of App.java, each of which contains different agents and is a separate ZooKeeper client).
* There are routes that implement the agent actions start_goal, notify_my_part_failed and notify_my_part_succeeded by writing to ZooKeeper znodes.
* There are two routes that monitor ZooKeeper znodes to detect when group goals have failed or succeeded, and send percepts expressing these facts to the agents.

