rem @echo off
net use H: \\storage.hcs-p01.otago.ac.nz\infosci-aamas\Stephen
H:

Setlocal EnableDelayedExpansion

if exist run_number.txt (
    cat run_number.txt
    for /f "delims=" %%n in ('cat run_number.txt') do set /a run_number=%%n+1
    echo !run_number! > run_number.txt
) else (
    set run_number=1
    echo 1 > run_number.txt
)

echo *** Run %run_number% >> %computername%_timing.log
w32tm /resync
echo * Stripchart * >> %computername%_timing.log
w32tm /stripchart /computer:student-ad-p05.student.otago.ac.nz /samples:1 >> %computername%_timing.log
echo >> %computername%_timing.log
