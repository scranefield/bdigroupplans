rem @echo off
rem net use H: \\storage.hcs-p01.otago.ac.nz\infosci-aamas\Stephen
H:

Setlocal EnableDelayedExpansion

if exist num_agents.txt (
    for /f "delims=" %%n in ('type num_agents.txt') do set num_agents=%%n
)

set run_number_file=run_number_%computername%.txt

if exist %run_number_file% (
    for /f "delims=" %%n in ('type %run_number_file%') do set /a run_number=%%n+1
    echo !run_number! > %run_number_file%
) else (
    set run_number=1
    echo 1 > %run_number_file%
)

set logfile=%num_agents%_agents_%computername%_run_%run_number%.log

w32tm /resync
echo * Stripchart * > %logfile%
w32tm /stripchart /computer:student-ad-p05.student.otago.ac.nz /samples:1 >> %logfile%
echo "-----" >> %logfile%

rem Run client
java -jar C:\Shared\Stephen\BDIGroupPlans-1.0-SNAPSHOT.jar >> %logfile% 2>&1
C:

