+Fact[source(self)]: shared_predicate(Fact) <-
    .println("*** Creating: ", Fact);
    zk_create(Fact);
    .println("*** Created: ", Fact).

-Fact[source(self)]: shared_predicate(Fact) <-
    .println("*** Deleting: ", Fact);
    zk_delete(Fact);
    .println("*** Deleted: ", Fact).

/*
+zk_removed(Agent, Percept) <-
   .println("Agent notified of removed percept ", Percept, " from ", Agent).

+zk_added(Agent, Percept) <-
   .println("Agent notified of added percept ", Percept, " from ", Agent).
*/
