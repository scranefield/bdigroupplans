// Generic rules supporting group goals

goal_for(Agent, [Goal|_], Goal) :-
  Goal =.. [_,_,Annots] &
  .member(agent(Agent), Annots).
goal_for(Agent, [_OtherGoal[agent(OtherAgent)]|Goals], Goal) :-
  OtherAgent \== Agent &
  goal_for(Agent, Goals, Goal).

remove_once([], _, []).
remove_once([H|T], ToRemove, T) :- H == ToRemove.
remove_once([H|T], ToRemove, L) :- H \== ToRemove & L = [H|T2] & remove_once(T, ToRemove, T2).

common_subgoal_list([], _, []).
common_subgoal_list([Agent|Agents], Subgoal, [SubgoalWithAgent|SubgoalsWithAgents]) :- 
    .add_annot(Subgoal, agent(Agent), SubgoalWithAgent) &
    common_subgoal_list(Agents, Subgoal, SubgoalsWithAgents).

has_annot(Term, Annot) :-
     Term =.. [_Functor, _Args, AnnotList] &
     .member(Annot, AnnotList).

plan_roles(PlanBodyTerms, Roles) :- plan_roles(PlanBodyTerms, [], Roles).

plan_roles([], Roles, Roles).
plan_roles([Goal|Goals], KnownRoles, Roles) :-
    goal_roles(Goal, KnownRoles, KnownRoles2) &
    plan_roles(Goals, KnownRoles2, Roles).

plan_body_roles(PlanBody, Roles) :-
    plan_body_roles(PlanBody, [], Roles).

plan_body_roles({}, Roles, Roles).
plan_body_roles({BodyTerm;BTs}, KnownRoles, Roles) :-
    body_term_roles(BodyTerm, KnownRoles, KnownRoles2) &
    plan_body_roles(BTs, KnownRoles2, Roles).

body_term_roles(BodyTerm, KnownRoles, KnownRoles) :-
    not BodyTerm = achieve(gg(_,_)).
body_term_roles(achieve(gg(_, SubGoalList)), KnownRoles, Roles) :-
    gg_subgoal_roles(SubGoalList, KnownRoles, Roles).

goal_roles(Goal, Roles, Roles) :-
    not Goal = body_term("achieve", gg(_,_)).
goal_roles(Goal, KnownRoles, Roles) :-
    Goal = body_term("achieve", gg(_, SubGoalList)) &
    gg_subgoal_roles(SubGoalList, KnownRoles, Roles).

gg_subgoal_roles([], Roles, Roles).
gg_subgoal_roles([SubGoal|SubGoals], KnownRoles, Roles) :-
    not has_annot(SubGoal, role(_)) &
    gg_subgoal_roles(SubGoals, KnownRoles, Roles).
gg_subgoal_roles([SubGoal|SubGoals], KnownRoles, Roles) :-
    has_annot(SubGoal, role(R)) &
    .union(KnownRoles, [R], KnownRoles2) &
    gg_subgoal_roles(SubGoals, KnownRoles2, Roles).

subst_list([], [], []).
subst_list([Role|Roles], [Agent|Agents], [subst(Role, Agent)|Substs]) :-
    subst_list(Roles, Agents, Substs).

agent_set(SubstList, Agents) :-
    agent_set(SubstList, [], Agents).

agent_set([], Agents, Agents).
agent_set([subst(_,Ag)|Substs], KnownAgents, Agents) :-
    .union([Ag], KnownAgents, KnownAgents2) &
    agent_set(Substs, KnownAgents2, Agents).

substitute_plan_body_roles(PlanBody, SubstList, NewPlanBody) :-
    agent_set(SubstList, Agents) &
    substitute_plan_body_roles(PlanBody, SubstList, Agents, NewPlanBody).

substitute_plan_body_roles({}, _, _, {}).
substitute_plan_body_roles({H;T}, SubstList, Agents, {H2;T2}) :-
    substitute_plan_body_term_roles(H, SubstList, Agents, H2) &
    substitute_plan_body_roles(T, SubstList, Agents, T2).

first_body_term({BT;_}, BT).

substitute_plan_body_term_roles(achieve(gg(Name, SubGoalList)), SubstList, Agents, BT) :-
    first_body_term({ !gg(Name, SubstSubGoalList) }, BT) &
    substitute_subgoal_roles(SubGoalList, SubstList, Agents, SubstSubGoalList).
substitute_plan_body_term_roles(BT, _ , _, BT) :-
    BT =.. [Type, Term, _] &
    (Type \== "achieve" | not Term = gg(_,_)).

substitute_subgoal_roles([], _, AgentsWithoutSubgoals, Noops) :-
    noop_actions(AgentsWithoutSubgoals, Noops).
substitute_subgoal_roles([SubGoal[role(R)]|SubGoals], SubstList, AgentsWithoutSubgoals, [SubGoal[agent(A)]|SubGoals2]) :-
    .if_then_else(.member(subst(R,A), SubstList),
                  {},
                  { .println("*** No agent is playing role ", R, " ***"); .fail; }) &
    .difference(AgentsWithoutSubgoals, [A], AgentsWithoutSubgoals2) &
    substitute_subgoal_roles(SubGoals, SubstList, AgentsWithoutSubgoals2, SubGoals2).

noop_actions([], []).
noop_actions([A|As], [noop[agent(A)]|Noops]) :-
    noop_actions(As, Noops).
