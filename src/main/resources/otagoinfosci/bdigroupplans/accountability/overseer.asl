{ include("gg_rules.asl") }

/* Domain-specific facts and rules */
join_timeout(_, 100000).
completion_timeout(_, 100000).
role_option(foundation_digger, a_1).
role_option(wall_builder, b_1).
role_option(overseer, overseer_1).
role_invitation_accepted(_Goal, _BodyTermList, overseer).

/* Initial goal */
!oversee_house_building.

/* Domain-specific plans */

+!oversee_house_building <-
    Goal = house_built;
    GoalTriggerEvent = {+!Goal[template]};
    .relevant_plans(GoalTriggerEvent, PlanList);
    !some_plan_succeeds(Goal, PlanList).

+!some_plan_succeeds(Goal, [Plan|_]) <-
    Plan = plan(_,_,_,PlanBody);
    ?plan_body_roles(PlanBody, PBRoles);
    !fill_roles(PBRoles, Goal, Plan, Agents);
    .println("All roles were filled by agents: ", Agents);
    ?subst_list(PBRoles, Agents, SubstList);
    if (substitute_plan_body_roles(PlanBody, SubstList, NewPlanBody)) {
        !begin_group_plan(Agents, Goal, NewPlanBody);
    } else {
        .println("Failed to fill all roles"); .fail;
    }.
-!some_plan_succeeds(Goal, [_|Plans]) <-
    !some_plan_succeeds(Goal, Plans).

+!fill_roles([], _, _, []).
+!fill_roles([Role|Roles], Goal, Plan, [Agent|Agents]) <-
    .findall(Candidate, role_option(Role, Candidate), Candidates);
    !choose_first_agreeing_candidate(Candidates, Goal, Plan, Role, Agent);
    !fill_roles(Roles, Goal, Plan, Agents).

+!choose_first_agreeing_candidate([Candidate|Candidates], Goal, Plan, Role, Agent) <-
    .send(Candidate, askOne, role_invitation_accepted(Goal, Plan, Role));
    .wait({+role_invitation_accepted(Goal, Plan, Role)}, 2000, EventTime);
    if (EventTime < 2000) {
        Agent = Candidate;
    } else {
        .println("Invitation timed out");
        !choose_first_agreeing_candidate(Candidates, Goal, Plan, Role, Agent);
    }.

+!begin_group_plan(Agents, Goal, NewPlanBody) : .my_name(Me) <-
    NewPlan = {+!Goal <- NewPlanBody};
    for (.member(Agent, Agents)) {
        if (Agent \== Me) {
            .send(Agent, tellHow, NewPlan);
            .send(Agent, achieve, Goal);
        }
    }
    .add_plan(NewPlan, self, begin);
    !Goal;
    .println("Goal succeeded!!!").

+started_goal(X,Y) <-
    .println("Received percept ", started_goal(X,Y)).

+!house_built[template] <-
    !gg(dummy, [foo[agent(noone)]]).
+!house_built[template] <-
    .my_name(Me);
    .println("*** Agent ", Me, " started the house building plan");
    !gg(build_foundations, [build_foundations[role(foundation_digger)], monitor[role(overseer)]]);
    !gg(build_walls, [build_walls[role(wall_builder)], monitor[role(overseer)]]).

+!monitor[gg(_GroupGoalName)].

{ include("gg_plans.asl") }

