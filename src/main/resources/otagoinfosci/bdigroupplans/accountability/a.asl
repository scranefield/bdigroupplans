// Include rules to support group goals
{ include("gg_rules.asl") }

// Domain-specific facts and rules
role_invitation_accepted(_Goal, _Plan, foundation_digger).
join_timeout(_, 100000).
completion_timeout(_, 100000).

// Domain-specific plans

+!build_foundations[gg(build_foundations)] <-
    work_on_foundations;
    .wait(500);
    work_on_foundations;
    .wait(500);
    work_on_foundations.
  
// Import plans supporting group goals
{ include("gg_plans.asl") }
