// Include rules to support group goals
{ include("gg_rules.asl") }

// Domain-specific facts and rules
role_invitation_accepted(_Goal, _Plan, wall_builder).

// Domain-specific plans

join_timeout(_, 100000).
completion_timeout(_, 100000).

+!build_walls[gg(build_walls)] <-
    work_on_walls;
    .wait(500);
    work_on_walls;
    .wait(500);
    work_on_walls.

// Import plans supporting group goals
{ include("gg_plans.asl") }
