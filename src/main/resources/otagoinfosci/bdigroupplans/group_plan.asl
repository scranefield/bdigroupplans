// Initial facts and rules
join_timeout(1000).
completion_timeout(1000).
group([a1, a2]).

// Initial goal
!start.

// Plans

{ begin group_plan }
+!start <-
  .println("Plan started")
  !gg(surround, [loc(a)[agent(a1)], loc(b)[agent(a2)]]),
  !gg(converge, [loc(c)[agent(a1)], loc(c)[agent(a2)]]);
  .println("Plan finished").

+!loc(a)[agent(a1), gg(surround)] <-
  .move_to(a).

+!loc(b)[agent(a2), gg(surround)] <-
  .move_to(b).

+!loc(c)[agent(a1), gg(surround)] <-
  .move_to(c).

+!loc(c)[agent(a2), gg(converge)] <-
  .move_to(c).
{ end }