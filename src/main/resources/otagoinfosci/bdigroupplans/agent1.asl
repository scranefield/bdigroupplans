// Use a hacked version of "include" to fix a Jason bug
{ my_include("group_plans.asl") }

// shared_predicate(loc(_)). // Not implemented currently

// Need to make sure these are the same for all agents!
join_timeout(surround, 1000).
join_timeout(converge, 1000).
completion_timeout(surround, 1000).
completion_timeout(converge, 1000).

// Initial goal
!start.

// Plans

+!start <-
  .println("Started");
  //.wait(400);
  !gg(surround, [loc(a)[agent(agent1_1)], loc(b)[agent(agent2_1)]]);
  !gg(converge, [loc(c)[agent(agent1_1)], loc(c)[agent(agent2_1)]]);
  .println("FINISHED GROUP PLAN").

+!start <-
  .println("2nd plan for start goal called. Why didn't .drop_intention affect the top-level intention !start?").

// Specific to agent
+!loc(L)[gg(GoalName)] : .member(GoalName, [surround, converge]) <-
  // move_to(L);
  // ?loc(L).
  //.wait(3000);
  +loc(L).

// Debugging:
^!gg(Name, Subgoals)[state(failed)] <-
  .println(gg(Name, Subgoals), " changed to state failed").

