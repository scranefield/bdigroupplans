// Rules

goal_for(Agent, [Goal|_], Goal) :-
  Goal =.. [_,_,Annots] &
  .member(agent(Agent), Annots).
goal_for(Agent, [_OtherGoal[agent(OtherAgent)]|Goals], Goal) :-
  OtherAgent \== Agent &
  goal_for(Agent, Goals, Goal).

// Generic plans supporting group goals

+!gg(Name, SubgoalList) : not already_failed(SubgoalList) & .my_name(Me) <-
  ?goal_for(Me, SubgoalList, MySubgoal);
  !!achieve_my_part(gg(Name, SubgoalList), MySubgoal);
  .suspend.

+!achieve_my_part(GroupGoal, MySubgoal) <-
  gg(GoalName, GoalList) = GroupGoal; 
  ?join_timeout(GoalName, JTO);
  ?completion_timeout(GoalName, CTO);
  start_goal(GoalList, MySubgoal, JTO, CTO); // Trigger the Start goal Camel route
  AnnotatedSubgoal = MySubgoal[gg(GoalName)];
  !AnnotatedSubgoal;
  notify_my_part_succeeded(GoalList, MySubgoal).

-!achieve_my_part(gg(_, GoalList), MySubgoal) <-
  notify_my_part_failed(GoalList, MySubgoal).

+succeeded(GoalList) <-
  .println("Notification: ", succeeded(GoalList));
  // For the next line, it would be better if Zookeeper recorded the goal name and sent it back in the succeeded percept
  .succeed_goal(gg(_, GoalList)).

+failed(GoalList) <-
   .println("Notification: ", failed(GoalList));
    // For the next line, it would be better if Zookeeper recorded the goal name and sent it back in the failed percept
   .fail_goal(gg(_, GoalList));
   // Does drop_intention work with a non-ground intention?
   .drop_intention(achieve_my_part(gg(_, GoalList), _));
   .drop_intention(gg(_, GoalList));
   // It would be nicer to use the goal name as the argument in already_failed (if we get it from Zookeeper)
   +already_failed(GoalList).
