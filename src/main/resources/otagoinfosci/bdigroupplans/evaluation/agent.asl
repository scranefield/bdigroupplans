// I used to use a hacked version of "include" to fix a Jason bug
// Currently I have pasted contents of group_plans.asl below (rules and plans separately) as loading from an "uber jar" didn't work
//{ my_include("group_plans.asl") }

// Generic rules supporting group goals
goal_for(Agent, [Goal|_], Goal) :-
  Goal =.. [_,_,Annots] &
  .member(agent(Agent), Annots).
goal_for(Agent, [_OtherGoal[agent(OtherAgent)]|Goals], Goal) :-
  OtherAgent \== Agent &
  goal_for(Agent, Goals, Goal).

join_timeout(main_goal, 100000).
completion_timeout(main_goal, 100000).

remove_once([], _, []).
remove_once([H|T], ToRemove, T) :- H == ToRemove.
remove_once([H|T], ToRemove, L) :- H \== ToRemove & L = [H|T2] & remove_once(T, ToRemove, T2).

common_subgoal_list([], _, []).
common_subgoal_list([Agent|Agents], Subgoal, [SubgoalWithAgent|SubgoalsWithAgents]) :- 
    .add_annot(Subgoal, agent(Agent), SubgoalWithAgent) &
    common_subgoal_list(Agents, Subgoal, SubgoalsWithAgents).
    
// Initial goal
!start.

// Plans

+!start
 // : .my_name(Name)
 <-
  //.println(Name, " started");
  register.

+registered_agents(L) : .member(dummy_agent, L) & not experiment_started & .my_name(MyName)
 <-
    +experiment_started;
    ?remove_once(L, dummy_agent, L2);
    ?common_subgoal_list(L2, subgoal, SubgoalList);
    //.println(Name, ": Subgoal list = ", SubgoalList);
    !gg(main_goal, SubgoalList);
    .println(MyName, ", succ_goal").

// Specific to agent
+!subgoal[gg(main_goal)]
 // : .my_name(Name)
 <-
    //.print("Subgoal called for ", Name);
    .random(N);
    .wait(N*5000);
    +subgoal.

// Debugging:
//^!gg(Name, Subgoals)[state(failed)] <-
//  .println(gg(Name, Subgoals), " changed to state failed").

// Generic plans supporting group goals

+!gg(Name, SubgoalList) : not already_failed(SubgoalList) & .my_name(Me) <-
  ?goal_for(Me, SubgoalList, MySubgoal);
  !!achieve_my_part(gg(Name, SubgoalList), MySubgoal);
  .suspend.

+!achieve_my_part(GroupGoal, MySubgoal) : .my_name(MyName) <-
  gg(GoalName, GoalList) = GroupGoal; 
  ?join_timeout(GoalName, JTO);
  ?completion_timeout(GoalName, CTO);
  start_goal(GoalList, MySubgoal, JTO, CTO); // Trigger the Start goal Camel route
  AnnotatedSubgoal = MySubgoal[gg(GoalName)];
  !AnnotatedSubgoal;
  notify_my_part_succeeded(GoalList, MySubgoal);
  .println(MyName, ", succ_subgoal"). // Should this be before the previous line, to include camel-agent processing in timing?

-!achieve_my_part(gg(_, GoalList), MySubgoal) <-
  notify_my_part_failed(GoalList, MySubgoal).

+succeeded(GoalList) <-
  //.println("Notification: ", succeeded(GoalList));
  // For the next line, it would be better if Zookeeper recorded the goal name and sent it back in the succeeded percept
  .succeed_goal(gg(_, GoalList)).

+failed(GoalList) <-
   .println("Notification: ", failed(GoalList));
    // For the next line, it would be better if Zookeeper recorded the goal name and sent it back in the failed percept
   .fail_goal(gg(_, GoalList));
   // Does drop_intention work with a non-ground intention?
   .drop_intention(achieve_my_part(gg(_, GoalList), _));
   .drop_intention(gg(_, GoalList));
   // It would be nicer to use the goal name as the argument in already_failed (if we get it from Zookeeper)
   +already_failed(GoalList).

