package otagoinfosci.bdigroupplans;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.spi.Registry;
import org.apache.camel.impl.PropertyPlaceholderDelegateRegistry;
import static org.apache.camel.builder.PredicateBuilder.not;
import camelagent.*;
import camelagent.util.SingletonContainerNamingStrategy;
import java.util.Properties;
import java.io.FileInputStream;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Multimap;
import com.google.common.collect.HashMultimap;
import java.util.HashSet;
import java.util.Set;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.zookeeper.ZooKeeperMessage;
import org.apache.camel.component.zookeeper.policy.ZooKeeperRoutePolicy;
import org.apache.camel.impl.SimpleRegistry;
import org.apache.zookeeper.data.Stat;

import jason.asSyntax.ASSyntax;
import jason.asSyntax.directives.DirectiveProcessor;
import jason.asSyntax.directives.Directive;
import org.apache.camel.LoggingLevel;

import static org.apache.camel.language.groovy.GroovyLanguage.groovy;

public class App {
    public static void main(String[] args) throws Exception {

        AgentContainer container;
        //((DirectiveProcessor) (container.getClass().getClassLoader().loadClass("jason.asSyntax.directives.DirectiveProcessor").newInstance())).addDirective("group_plan", new GroupPlanDirective());
        //DirectiveProcessor.addDirective("group_plan", new GroupPlanDirective());
        //DirectiveProcessor.addDirective("my_include", new MyInclude());
        container = new AgentContainer(new SingletonContainerNamingStrategy(),
                                       App.class.getClassLoader(),
                                       App.class.getPackage());
	final CamelContext camel = new DefaultCamelContext(new SimpleRegistry());
        
        
        camel.addComponent("agent", new AgentComponent(container));

        //Multimap watches =  Multimaps.synchronizedSetMultimap(HashMultimap.create());
        Set failedGoals = new HashSet();
        Set successfulGoals = new HashSet();       
        
        Registry registry = camel.getRegistry();
        if (registry instanceof PropertyPlaceholderDelegateRegistry)
            registry = ((PropertyPlaceholderDelegateRegistry)registry).getRegistry();
        //((SimpleRegistry) registry).put("watches", watches);
        //((SimpleRegistry) registry).put("cachedState", cachedState);
        ((SimpleRegistry) registry).put("failedGoals", failedGoals);
        ((SimpleRegistry) registry).put("successfulGoals", successfulGoals);
        
        Properties props = new Properties();
        String zkServerProp;
	try {
            props.load(new FileInputStream("config.properties"));
            zkServerProp = props.getProperty("zookeeper_server");
	} catch (Exception e) {
	  zkServerProp = "127.0.0.1:2181";
	}
	final String zkserver = zkServerProp;
        
	/* Create the routes */
	camel.addRoutes(new RouteBuilder() {
            @Override
	    public void configure() {
               
                ZooKeeperRoutePolicy leaderOnlyPolicy = new ZooKeeperRoutePolicy("zookeeper://" + zkserver + "/leader", 1);
                
                // Master route - maintains state machine for each group goal
                from("timer:master_tick?period=200&fixedRate=true").routeId("master-split-subgoal-list")
                .routePolicy(leaderOnlyPolicy) // Only one node will run this route, based on a leadership election
                .log(LoggingLevel.DEBUG, "Master TICK")
                // The next line didn't work for more than one timer tick:
                // .pollEnrich("zookeeper://" + zkserver + "/active?listChildren=true")
                .process(new Processor() {
                         public void process(Exchange exchange) throws Exception {
                           ConsumerTemplate consumer = camel.createConsumerTemplate();
                           Exchange pollResult = consumer.receive("zookeeper://" + zkserver + "/active?listChildren=true");
                           exchange.setOut(pollResult.getIn());
                         }})
                .split(body())
                .to("direct:check_timeout");
                
                // Continuation of master route
                from("direct:check_timeout").routeId("master-check-timeouts")
                .log(LoggingLevel.DEBUG, "Checking goal for expiry: ${body}")
                .setHeader("goal", body())
                .process(new Processor() {
                         public void process(Exchange exchange) throws Exception {
                           long currentTime = System.currentTimeMillis();
                           //System.out.println("Current time: " + currentTime);
                           ConsumerTemplate consumer = camel.createConsumerTemplate();
                           Exchange goalQueryResult = consumer.receive("zookeeper://" + zkserver + "/active/" +
                                                                  exchange.getIn().getBody());
                           Message goalResponseMsg = goalQueryResult.getIn();
                           GoalData goalData = GoalData.fromJson(goalResponseMsg.getBody(String.class));
                           //System.out.println("Goal data from zk: " + goalData);
                           Stat goalStats = goalResponseMsg.getHeader("CamelZookeeperStatistics", Stat.class);
                           
                           Exchange joinedQueryResult = consumer.receive("zookeeper://" + zkserver + "/active/" +
                                                                  exchange.getIn().getBody() + "/joined");
                           Message joinedResponseMsg = joinedQueryResult.getIn();
                           Stat joinedStats = joinedResponseMsg.getHeader("CamelZookeeperStatistics", Stat.class);
                           
                           Exchange failedQueryResult = consumer.receive("zookeeper://" + zkserver + "/active/" +
                                                                  exchange.getIn().getBody() + "/failed");
                           Message failedResponseMsg = failedQueryResult.getIn();
                           Stat failedStats = failedResponseMsg.getHeader("CamelZookeeperStatistics", Stat.class);
                           
                           Exchange succeededQueryResult = consumer.receive("zookeeper://" + zkserver + "/active/" +
                                                                  exchange.getIn().getBody() + "/succeeded");
                           Message succeededResponseMsg = succeededQueryResult.getIn();
                           Stat succeededStats = succeededResponseMsg.getHeader("CamelZookeeperStatistics", Stat.class);
                           
                           long lastModifiedTime = goalStats.getMtime();
                           com.google.gson.Gson gson = new com.google.gson.Gson();
                           String statsGson  = gson.toJson(goalStats);
                           //System.out.println("Goal stats from zk: " + statsGson);
                           
                           Message newMsg = exchange.getIn(); // Will be automatically copied to be exchange out message
                           newMsg.setBody(goalData);
                           newMsg.setHeader("numSubGoals", goalData.getNumSubgoals());
                           newMsg.setHeader("currentTime", currentTime);
                           newMsg.setHeader("failedCount", failedStats.getNumChildren());
                           newMsg.setHeader("succeededCount", succeededStats.getNumChildren());
                           newMsg.setHeader("alreadyFailed", goalData.isFailed());
                           newMsg.setHeader("alreadySucceeded", goalData.isSucceeded());
                           newMsg.setHeader("allJoined", joinedStats.getNumChildren() == goalData.getNumSubgoals());
                           newMsg.setHeader("joinTimedOut",
                                            !((Boolean) newMsg.getHeader("allJoined")) &&
                                            currentTime > goalStats.getCtime() + goalData.getJoinTimeout());
                           newMsg.setHeader("allJoinedTime", goalData.getAllJoinedTime());
                           newMsg.setHeader("completionTimedOut", goalData.getAllJoinedTime() != -1 && currentTime > goalData.getAllJoinedTime() + goalData.getCompletionTimeout());
                           newMsg.setHeader("lastModifiedTime", lastModifiedTime);
                         }})
                //.log("Goal status: joinTimedOut = ${header.joinTimedOut}; allJoined = ${header.allJoined}; allJoinedTime = ${header.allJoinedTime}; completionTimedOut = ${header.completionTimedOut}; alreadyFailed = ${header.alreadyFailed}")
                .choice()
                    .when(header("alreadyFailed"))
                        .log(LoggingLevel.DEBUG, "Already failed")
                    .when(header("alreadySucceeded"))
                        .log(LoggingLevel.DEBUG, "Already succeeded")
                    .when(header("succeededCount").isEqualTo(header("numSubGoals")))
                        .log("Succeeded")
                        .setBody(groovy("request.body.setSucceeded(true);" +
                                        "request.body"))
                        .to("direct:zk_update_goal_data", "direct:notify_successes")
                    .when(header("failedCount").isGreaterThanOrEqualTo(1))
                        .log("Subgoal failed")
                        .setBody(groovy("request.body.setFailed(true);" +
                                        "request.body"))
                        .to("direct:zk_update_goal_data", "direct:notify_failures")
                    .when(header("joinTimedOut"))
                        .log("Join timed out")
                        .setBody(groovy("request.body.setFailed(true);" +
                                        "request.body"))
                        .to("direct:zk_update_goal_data", "direct:notify_failures")
                    .when(header("allJoined"))
                        .log("All joined")
                        .choice()
                            .when(header("allJoinedTime").isEqualTo(-1))
                                .setBody(groovy("request.body.setAllJoined(true); " +
                                                "request.body.setAllJoinedTime(request.headers.lastModifiedTime);" +
                                                "request.body"))
                                .setHeader("updateNeeded", constant(true))
                        .endChoice()
                        .choice()
                            .when(groovy("request.headers.currentTime > request.body.allJoinedTime + request.body.completionTimeout"))
                                .log("Detected completion timeout")
                                .setBody(groovy("request.body.setFailed(true); " +
                                                "request.body"))
                                .setHeader("updateNeeded", constant(true))
                                .setHeader("newFailure", constant(true))
                        .endChoice()
                        .choice()
                            .when(header("updateNeeded"))
                                .to("direct:zk_update_goal_data")
                        .endChoice()
                        .choice()
                            .when(header("newFailure"))
                                .to("direct:notify_failures")
                        .endChoice()
                        
                    .otherwise()
                        .log("Otherwise case");                    
                        
                // Continuation of master route
                from("direct:zk_update_goal_data").routeId("master-update-goal-data")
                // Note: Body is a GoalData object at this point - need to convert to Json below
                .log(LoggingLevel.DEBUG, "Writing ${body} to zookeeper://" + zkserver + "/active/${header.goal}")
                // RecipientList doesn't seem to send body:
                //  .recipientList(simple("zookeeper://" + zkserver + "/active/${header.goal}"), "#")
                // Replaced with:
                .process(new Processor() {
                            public void process(Exchange exchange) throws Exception {
                                ProducerTemplate producer = camel.createProducerTemplate();
                                producer.sendBody("zookeeper://" + zkserver + "/active/" + exchange.getIn().getHeader("goal") + "?create=true&createMode=PERSISTENT",
                                                  exchange.getIn().getBody(GoalData.class).toJson());  
                            }
                        });
                
                // Master route continued
                from("direct:notify_failures").routeId("master-notify-failures")
                .log(LoggingLevel.DEBUG, "About to create failure node for ${header.goal}")
                // Why doesn't recipient list work?
                //recipientList(simple("zookeeper://" + zkserver + "/failed/${header.goal}?create=true&createMode=PERSISTENT"), "#");
                .process(new Processor() {
                            public void process(Exchange exchange) throws Exception {
                                ProducerTemplate producer = camel.createProducerTemplate();
                                producer.sendBody("zookeeper://" + zkserver + "/failed/" + exchange.getIn().getHeader("goal") + "?create=true&createMode=PERSISTENT",
                                                  "Don't care");  
                            }
                        });
                
                // Master route continued
                from("direct:notify_successes").routeId("master-notify-successes")
                .log(LoggingLevel.DEBUG, "About to create success node for ${header.goal}")
                // Why doesn't recipient list work?
                //recipientList(simple("zookeeper://" + zkserver + "/succeeded/${header.goal}?create=true&createMode=PERSISTENT"), "#");
                .process(new Processor() {
                            public void process(Exchange exchange) throws Exception {
                                ProducerTemplate producer = camel.createProducerTemplate();
                                producer.sendBody("zookeeper://" + zkserver + "/succeeded/" + exchange.getIn().getHeader("goal") + "?create=true&createMode=PERSISTENT",
                                                  "Don't care");  
                            }
                        });
                
                // Implement start_goal action
                from("agent:action?actionName=start_goal").routeId("implement-start_goal")
                // TO DO: Check that actor is involved in the goal!
                .setBody(groovy("int numSubgoals = jason.asSyntax.ASSyntax.parseList(request.headers.params[0]).size();" +
                                "new otagoinfosci.bdigroupplans.GoalData(" +
                                   "numSubgoals," +
                                   "request.headers.params[2].asType(Integer)," +
                                   "request.headers.params[3].asType(Integer)," +
                                   "numSubgoals == 1," +
                                   "false, " +
                                   "false" +
                                ").toJson()"))
                .log(LoggingLevel.DEBUG, "Body for zk goal node: ${body}")
                .recipientList(simple("zookeeper://" + zkserver + "/active/${header.params[0]}?create=true&createMode=PERSISTENT"), "#") // Delimeter # not used, but we can't have the default value ","
                .recipientList(simple("zookeeper://" + zkserver + "/active/${header.params[0]}/joined?create=true&createMode=PERSISTENT"), "#") // Delimeter # not used, but we can't have the default value ","
                .recipientList(simple("zookeeper://" + zkserver + "/active/${header.params[0]}/failed?create=true&createMode=PERSISTENT"), "#") // Delimeter # not used, but we can't have the default value ","
                .recipientList(simple("zookeeper://" + zkserver + "/active/${header.params[0]}/succeeded?create=true&createMode=PERSISTENT"), "#") // Delimeter # not used, but we can't have the default value ","
                .setBody(constant("Don't care"))
                .recipientList(simple("zookeeper://" + zkserver + "/active/${header.params[0]}/joined/${header.params[1]}?create=true&createMode=PERSISTENT"), "#");
                
                // Send failure percepts to agents when a group goal has failed
                from("zookeeper://" + zkserver + "/failed?listChildren=true&repeat=true").routeId("generate-failure-percepts")
                .log(LoggingLevel.DEBUG, "Children of node 'failed': ${body} (type: ${body.class})")
                .choice()
                    .when(not(simple("${body.isEmpty()}")))
                        .setHeader("newFailedGoals", groovy("new HashSet(request.body) - camelContext.registry.lookupByName('failedGoals')"))
                        .log(LoggingLevel.DEBUG, "New failed goals: ${header.newFailedGoals}")
                        .beanRef("failedGoals", "addAll(${body})") // Sets body to result as an unwanted side effect
                        .split(header("newFailedGoals"))
                        .log(LoggingLevel.DEBUG, "Body after split: ${body}")
                        // Next line commented out as need to send to ALL agents, not just those involved in goal
                        //.setHeader("receiver", groovy("'agent:percept?receiver=' << (request.body.findAll(/source\\((.+?)\\)/) { match, agent -> agent }).unique().join(',')"))
                        .setBody(simple("failed(${body})"))
                        .log(LoggingLevel.DEBUG, "Failure notification from zk: ${body}")
                        .to("agent:percept"); 
                
                // Send success percepts to agents when a group goal has failed
                from("zookeeper://" + zkserver + "/succeeded?listChildren=true&repeat=true").routeId("generate-success-percepts")
                .log(LoggingLevel.DEBUG, "Children of node 'succeeded': ${body} (type: ${body.class})")
                .choice()
                    .when(not(simple("${body.isEmpty()}")))
                        .setHeader("newSuccessfulGoals", groovy("new HashSet(request.body) - camelContext.registry.lookupByName('successfulGoals')"))
                        .log(LoggingLevel.DEBUG, "New successful goals: ${header.newSuccessfulGoals}")
                        .beanRef("successfulGoals", "addAll(${body})") // Sets body to result as an unwanted side effect
                        .split(header("newSuccessfulGoals"))
                        .log(LoggingLevel.DEBUG, "Body after split: ${body}")
                        // Next line commented out as need to send to ALL agents, not just those involved in goal
                        //.setHeader("receiver", groovy("'agent:percept?receiver=' << (request.body.findAll(/source\\((.+?)\\)/) { match, agent -> agent }).unique().join(',')"))
                        .setBody(simple("succeeded(${body})"))
                        .log(LoggingLevel.DEBUG, "Success notification from zk: ${body}")
                        .to("agent:percept"); 
                
                // Implement notify_my_part_failed action
                from("agent:action?actionName=notify_my_part_failed").routeId("implement-notify_my_part_failed")
                .log(LoggingLevel.DEBUG, "notify_my_part_failed received for subgoal ${header.params[1]}")
                .setBody(constant("Don't care"))
                .recipientList(simple("zookeeper://" + zkserver + "/active/${header.params[0]}/failed/${header.params[1]}?create=true&createMode=PERSISTENT"), "#");
          
                // Implement notify_my_part_succeeded action
                from("agent:action?actionName=notify_my_part_succeeded").routeId("implement-notify_my_part_succeeded")
                .log(LoggingLevel.DEBUG, "notify_my_part_succeeded received for subgoal ${header.params[1]}")
                .setBody(constant("Don't care"))
                .recipientList(simple("zookeeper://" + zkserver + "/active/${header.params[0]}/succeeded/${header.params[1]}?create=true&createMode=PERSISTENT"), "#");
          
                // Legacy routes for handling shared percepts
                
//                from("agent:action?actionName=zk_create")
//                .log("Creating node: ${header.params[0]}")
//                .recipientList(simple("zookeeper://" + zkserver + "/state/${header.actor}~${header.params[0]}?create=true&createMode=PERSISTENT"));                
//                
//                from("agent:action?actionName=zk_delete")
//                .log("Deleting node: ${header.params[0]}")
//                .recipientList(simple("zookeeper://" + zkserver + "/state/${header.actor}~DELETED~${header.params[0]}?create=true&createMode=PERSISTENT"))
//                .log("Just added ${header.actor}~DELETED~${header.params[0]}")
//                .setHeader(ZooKeeperMessage.ZOOKEEPER_OPERATION, constant("DELETE"))
//                .recipientList(simple("zookeeper://" + zkserver + "/state/${header.actor}~${header.params[0]}"));
//                
//                from("agent:action?actionName=zk_watch") // 1st arg.: WatchedAgent; 2nd arg.: RegExp
//                .setProperty("watchKey", groovy("\"${request.headers.params[0]}~${request.headers.params[1][1..-2]}\""))
//                .log("Asked to watch ${property.watchKey}")
//                .choice()
//                   .when(not(method("watches", "containsKey(${property.watchKey})")))
//                        .log("Not in watches - adding it")
//                        .beanRef("watches", "put(${property.watchKey}, ${header.actor})")
//                   .otherwise()
//                        .log("In watches - nothing to do")
//                .end();
//                
//                from("agent:action?actionName=zk_unwatch")
//                .setProperty("watchKey", simple("${header.params[0]}~${header.params[1]}"))
//                .beanRef("watches", "remove(${property.watchKey}, ${header.actor})");
//                
//                from("zookeeper://" + zkserver + "/state?listChildren=true&repeat=true")
//                .process(new Processor() {
//                         public void process(Exchange exchange) throws Exception {
//                           ConsumerTemplate consumer = camel.createConsumerTemplate();
//                           String children = consumer.receiveBody("zookeeper://"  +zkserver + "/state?listChildren=true", String.class);
//                           System.out.println(children);
//                           //exchange.getIn().setBody(agentName);
//                         }})
//                .log("Zookeeper update: ${body}")
//                .setProperty("deletionFacts", groovy("request.body.inject([] as Set)"
//                                                     + " { facts, fact -> fact.contains('DELETED~') ? facts << (fact - 'DELETED~') : facts }"))
//                .log("Deletion facts: ${property.deletionFacts}")
//                //.setProperty("newFacts", groovy("com.google.common.collect.Sets.difference(new HashSet(request.body), camelContext.registry.lookupByName('cachedState')).immutableCopy()"))
//                .setProperty("newFacts", groovy("(new HashSet(request.body) - (camelContext.registry.lookupByName('cachedState'))).findAll { it.split('~')[1] != 'DELETED' }"))        
//                .log("New facts: ${property.newFacts}")
//                //.setProperty("removedFacts", groovy("com.google.common.collect.Sets.difference(camelContext.registry.lookupByName('cachedState'), new HashSet(request.body)).immutableCopy()"))
//                .setProperty("removedFacts", groovy("camelContext.registry.lookupByName('cachedState') - new HashSet(request.body) + exchange.getProperty('deletionFacts')"))        
//                .log("Removed facts: ${property.removedFacts}")
//                //.setProperty("newFacts", groovy("camelContext.registry.lookupByName('cachedState').findAll { it.split('~')[1] != 'DELETED' }"))
//                //.log("New facts: ${property.newFacts}")
//                .log("state set: ${ref:cachedState}")
//                .beanRef("cachedState", "removeAll(${property.removedFacts})")
//                .beanRef("cachedState", "addAll(${property.newFacts})")
//                .log("Updated state set: ${ref:cachedState}")
//                .choice().when(not(simple("${property.deletionFacts.empty}")))
//                  .setProperty("deletionURLs",
//                                groovy("exchange.getProperty('deletionFacts').inject('', " +
//                                        " { recipListStr, factStr -> " +
//                                             "String[] splits = factStr.split('~', 2); " +
//                                             "recipListStr << " +
//                                             "\"zookeeper://" + zkserver + "/state/${splits[0]}~DELETED~${splits[1]},\"" +
//                                        " })[0..-2]"))  // Use upper index -2 to omit final comma
//                  .to("direct:deleteDeletionFacts")
//                .end()
//                .setBody(groovy("def multimap = exchange.getProperty('newFacts').inject(com.google.common.collect.HashMultimap.create(), { prev, fact -> " +
//                                "  prev.putAll('zk_added(' + fact.replaceFirst('~', {','}) + ')', " +
//                                "              camelContext.registry.lookupByName('watches').asMap().inject(" +
//                                "                [] as Set, " +
//                                "                { s, regexp, watchers -> (fact ==~ regexp ? s + watchers : s) } " +
//                                "             )); " +
//                                "  return prev" +
//                                "}); " +
//                                "multimap.putAll( " +
//                                "exchange.getProperty('removedFacts').inject(com.google.common.collect.HashMultimap.create(), { prev, fact -> " +
//                                "  prev.putAll('zk_removed(' + fact.replaceFirst('~', {','}) + ')', " +
//                                "              camelContext.registry.lookupByName('watches').asMap().inject(" +
//                                "                [] as Set, " +
//                                "                { s, regexp, watchers -> (fact ==~ regexp ? s + watchers : s) } " +
//                                "             )); " +
//                                "  return prev" +
//                                "})); " +
//                                "return com.google.common.collect.Multimaps.invertFrom(multimap, com.google.common.collect.HashMultimap.create()).asMap().entrySet()"))
//                .log("New multiset entries: ${body}")
//                /*
//                .setBody(groovy("exchange.getProperty('newFacts').collectEntries { fact -> " +
//                                " [ 'zk_added(' + fact.replaceFirst('~', {','}) + ')', " +
//                                "   camelContext.registry.lookupByName('watches').asMap().inject(" +
//                                "     [] as Set, " +
//                                "     { s, regexp, watchers -> (fact ==~ regexp ? s + watchers : s) } " +
//                                "   ) " +
//                                " ]} " +
//                                "<< " +
//                                "exchange.getProperty('removedFacts').collectEntries { fact -> " +
//                                " [ 'zk_removed(' + fact.replaceFirst('~', {','}) + ')', " +
//                                "   camelContext.registry.lookupByName('watches').asMap().inject(" +
//                                "     [] as Set, " +
//                                "     { s, regexp, watchers -> (fact ==~ regexp ? s + watchers : s) } " +
//                                "   ) " +
//                                " ]} "))
//                .log("Updates for agents: ${body}")
//                */
//                .split(body())
//                .to("direct:sendPercepts");
//                
//                from("direct:deleteDeletionFacts")
//                .log("direct:deleteDeletionFacts route")
//                .log("Deletion URLs: ${property.deletionURLs}")
//                // The next two lines didn't work:
//                //   .setHeader(ZooKeeperMessage.ZOOKEEPER_OPERATION, constant("DELETE"))
//                //   .recipientList(simple("${property.deletionURLs}"));
//                .process(new Processor() {
//                         public void process(Exchange exchange) throws Exception {
//                           ProducerTemplate producer = camel.createProducerTemplate();
//                           producer.sendBodyAndHeader((String) exchange.getProperty("deletionURLs"), "DummyValue", ZooKeeperMessage.ZOOKEEPER_OPERATION, "DELETE");
//                         }});
//                
//                from("direct:sendPercepts")
//                // .setBody(simple("${body.value}"))  // Causes exception - bug in Camel?
//                .setBody(groovy("request.body.value"))
//                .setHeader("receiver", groovy("request.body.key"))
//                .to("agent:percept");
                
            }
        });
        
        // turn exchange tracing on or off (false is off)
        //camel.setTracing(true);

	
        // create root ZooKeeper parent node for state
        ProducerTemplate template = camel.createProducerTemplate();
        //template.sendBody("zookeeper://" + zkserver + "/state?create=true&createMode=PERSISTENT", "Don't care");
        template.sendBody("zookeeper://" + zkserver + "/active?create=true&createMode=PERSISTENT", "Don't care");
        //template.sendBody("zookeeper://" + zkserver + "/timed_out?create=true&createMode=PERSISTENT", "Don't care");
        template.sendBody("zookeeper://" + zkserver + "/failed?create=true&createMode=PERSISTENT", "Don't care");
        template.sendBody("zookeeper://" + zkserver + "/succeeded?create=true&createMode=PERSISTENT", "Don't care");
        template.sendBody("zookeeper://" + zkserver + "/notified?create=true&createMode=PERSISTENT", "Don't care");
        template.sendBody("zookeeper://" + zkserver + "/leader?create=true&createMode=PERSISTENT", "Don't care");

        // start routing
        camel.start();
	System.out.println("Starting router...");
        
        // Need time for Zookeeper Master election
        Thread.sleep(2000);

	//Start the agents after starting the routes
        container.startAllAgents();

	System.out.println("... ready.");
    }            
                
}
