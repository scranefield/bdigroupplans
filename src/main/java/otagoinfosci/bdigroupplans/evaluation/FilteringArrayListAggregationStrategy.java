package otagoinfosci.bdigroupplans.evaluation;

import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.apache.camel.Exchange;
import java.util.ArrayList;

public class FilteringArrayListAggregationStrategy implements AggregationStrategy {
     String filterString = null;
     
     public FilteringArrayListAggregationStrategy(String filterString) {
         this.filterString = filterString;
     }

     public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
 	Object newBody = newExchange.getIn().getBody();
        if (!newBody.toString().contains(filterString)) return oldExchange;
        
        ArrayList<Object> list = null;
         if (oldExchange == null) {
 		list = new ArrayList<Object>();
 		list.add(newBody);
 		newExchange.getIn().setBody(list);
 		return newExchange;
         } else {
 	        list = oldExchange.getIn().getBody(ArrayList.class);
 		list.add(newBody);
 		return oldExchange;
 	}
     }
 }