package otagoinfosci.bdigroupplans.evaluation;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.dataformat.csv.CsvDataFormat;
import static org.apache.camel.language.groovy.GroovyLanguage.groovy;

public class DataCollector {
   public static void main(String[] args) throws Exception {

      // create default context
      final CamelContext camel = new DefaultCamelContext();

      camel.addRoutes(new RouteBuilder() {
         public void configure() throws Exception {

            from("file:data")
               .setHeader("NumAgents", groovy("request.headers.get('CamelFileNameOnly').find(/\\d+/)"))
               .setHeader("ComputerName", groovy("request.headers.get('CamelFileNameOnly').find(/SOB\\d+/)"))
               .setHeader("RunNumber", groovy("request.headers.get('CamelFileNameOnly').findAll(/\\d+/)[-1]"))
               .split(body().tokenize("\n"), new FilteringArrayListAggregationStrategy("succ_"))
                  .transform(groovy("data = request.body.split(', '); " +
                                    "data.size() > 1 ? " +
                                    "  [a: request.headers.get('NumAgents'), " +
                                    "   b: request.headers.get('RunNumber'), " +
                                    "   c: request.headers.get('ComputerName')+'_'+data[0].findAll(/\\d+/)[-1], " +
                                    "   d: data[1], " +
                                    "   e: data[2].trim() ] " +
                                    " : [:]"))
               .end()
               .log("Aggregated: ${body}")
               .marshal().csv()
               .to("file:dataout?fileName=${file:name.noext}.csv");
          }
      });

      camel.setTracing(Boolean.FALSE);

      System.out.println("Starting router...");

      camel.start();
      Thread.currentThread().join();

      System.out.println("... ready.");
   }
}
