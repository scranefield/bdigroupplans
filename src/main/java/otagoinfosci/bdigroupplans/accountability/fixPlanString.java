package otagoinfosci.bdigroupplans.accountability;

import jason.JasonException;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.StringTerm;
import jason.asSyntax.Term;


/**
 *
 * @author crast78p
 */
public class fixPlanString extends DefaultInternalAction {
    
    @Override public int getMinArgs() { return 2; }
    @Override public int getMaxArgs() { return 2; }
    
    @Override protected void checkArguments(Term[] args) throws JasonException {
        super.checkArguments(args); // check number of arguments
        if (!(args[0] instanceof StringTerm))
            throw JasonException.createWrongArgument(this, "The first argument must be a StringTerm.");
        if (!(args[1] instanceof StringTerm))
            throw JasonException.createWrongArgument(this, "The second argument must be a StringTerm.");
    }
    
    @Override public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
        String s = args[0].toString();
        if (s.startsWith("\"{ ") && s.endsWith(" }\"")) {
            s = s.substring(3).substring(0, s.length()-6);
            s = s.replaceAll("(_[0-9]+)+([A-Z])", "$2"); // Fix Jason 1.3.9 problem with vars converted to anonymous vars
            s = s.replace("\"", "\\\"") + ".";
        }
        return un.unifies(ASSyntax.createString(s), args[1]);
    } 
}
