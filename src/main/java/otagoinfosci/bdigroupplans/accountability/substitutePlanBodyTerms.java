package otagoinfosci.bdigroupplans.accountability;

import jason.JasonException;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.ListTerm;
import jason.asSyntax.Literal;
import jason.asSyntax.Plan;
import jason.asSyntax.PlanBody;
import jason.asSyntax.PlanBody.BodyType;
import jason.asSyntax.Term;
import jason.asSyntax.Trigger;

public class substitutePlanBodyTerms extends DefaultInternalAction {
    
    @Override public int getMinArgs() { return 4; }
    @Override public int getMaxArgs() { return 4; }
    
    @Override protected void checkArguments(Term[] args) throws JasonException {
        super.checkArguments(args); // check number of arguments
        if (!(args[0] instanceof Plan))
            throw JasonException.createWrongArgument(this, "The first argument must be a Plan.");
        if (!(args[1].isList()))
            throw JasonException.createWrongArgument(this, "The second argument must be a Jason list.");
        if (!(args[2].isList()))
            throw JasonException.createWrongArgument(this, "The third argument must be a Jason list.");
        if (!(args[3].isVar()))
            throw JasonException.createWrongArgument(this, "The second argument must be a Jason variable.");
    }
    
    @Override public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
        checkArguments(args);
        Plan oldP = (Plan) args[0];
        Trigger oldT = oldP.getTrigger();
        Literal litNoTemplateAnnot = oldT.getLiteral().copy();
        litNoTemplateAnnot.delAnnot(ASSyntax.createLiteral("template"));
        Trigger t = new Trigger(oldT.getOperator(), oldT.getType(), litNoTemplateAnnot);
        Plan plan = new Plan(null, t, oldP.getContext(), oldP.getBody()); // Note label replaced with null
        plan.setAsPlanTerm(true);
        PlanBody pb = plan.getBody();
        int numTerms = pb.getPlanSize();
        ListTerm oldBodyTermList = (ListTerm) args[1];
        ListTerm newBodyTermList = (ListTerm) args[2];
        for (int i=0; i < numTerms; i++) {
            Term oldBodyTerm = oldBodyTermList.get(i);
            Term newBodyTerm = newBodyTermList.get(i);
            Literal oldBodyLiteral = null, newBodyLiteral = null;            
            if (oldBodyTerm instanceof Literal) {
                oldBodyLiteral = (Literal) oldBodyTerm;
            } else {
                throw JasonException.createWrongArgument(this, "List elements of the second argument must be literals");
            }
            if (newBodyTerm instanceof Literal) {
                newBodyLiteral = (Literal) newBodyTerm;
            } else {
                throw JasonException.createWrongArgument(this, "List elements of the second argument must be literals");
            }
            if (!oldBodyLiteral.equals(newBodyLiteral)) {
                this.updateBodyTerm(pb, newBodyLiteral);
            }
            pb = pb.getBodyNext();
        }
        return un.unifies(plan, args[3]);
    } 
    
    private void updateBodyTerm(PlanBody pb, Literal l) {
        String bodyTypeString = l.getTerm(0).toString().replace("\"", "");
        pb.setBodyType(BodyType.valueOf(bodyTypeString));
        pb.setBodyTerm(l.getTerm(1));
    }
}
