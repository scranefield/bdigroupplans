/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otagoinfosci.bdigroupplans.accountability;

import jason.JasonException;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.ListTerm;
import jason.asSyntax.Term;
import jason.asSyntax.Plan;
import jason.asSyntax.PlanBody;


/**
 *
 * @author crast78p
 */
public class planBodyTerms extends DefaultInternalAction {
    
    @Override public int getMinArgs() { return 2; }
    @Override public int getMaxArgs() { return 2; }
    
    @Override protected void checkArguments(Term[] args) throws JasonException {
        super.checkArguments(args); // check number of arguments
        if (!(args[0] instanceof Plan))
            throw JasonException.createWrongArgument(this,"The first argument must be a Plan");
    }
    
    @Override public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
        checkArguments(args);
        Plan plan = (Plan) args[0];
        PlanBody pb = plan.getBody();
        int numTerms = pb.getPlanSize();
        ListTerm lt = ASSyntax.createList();
        for (int i=0; i < numTerms; i++) {
            lt.add(ASSyntax.createLiteral(
                "body_term",
                ASSyntax.createString(pb.getBodyType().name()),
                pb.getBodyTerm()));
            pb = pb.getBodyNext();
        }
        
        return un.unifies(lt, args[1]);
    } 
}
