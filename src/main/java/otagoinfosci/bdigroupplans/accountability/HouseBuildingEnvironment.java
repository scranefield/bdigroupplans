package otagoinfosci.bdigroupplans.accountability;
import jason.asSyntax.*;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class HouseBuildingEnvironment extends jason.environment.Environment {
 
    private int foundations_quality = 0;
    private int wall_quality = 0;
    private Boolean envChanged;
    private Set<String> agentNames = Collections.EMPTY_SET;
    
    public synchronized Boolean isEnvChanged() { // Use Boolean so it can be in a Camel message body
        return this.envChanged;
    }
    
    public void setAgentNames(Set<String> agentNames) {
        this.agentNames = agentNames;
    }   
    
    public void updatePercepts() {
        clearPercepts();
        Literal fq = Literal.parseLiteral(String.format("foundations_quality(%d)", foundations_quality));
        Literal wq = Literal.parseLiteral(String.format("wall_quality(%d)", wall_quality));
        addPercept(fq);
        addPercept(wq);        
    }
    
    public void init() {
        this.envChanged = Boolean.TRUE;
        updatePercepts();
    }
    
    public synchronized boolean executeActionString(String ag, String act) {
        int orig_foundations_quality = this.foundations_quality;
        int orig_wall_quality = this.wall_quality;
            
        String functor = act;
        if (functor.equals("work_on_foundations")) {
            this.foundations_quality = Math.min(this.foundations_quality+1, 3);
        }  
        else if (functor.equals("work_on_walls") && this.foundations_quality >= 2) {
            this.wall_quality = Math.min(this.wall_quality+1, 3);
        }
        else {
            System.out.println("Action not supported by environment: " + act);
            return false;
        }
        if ((this.foundations_quality != orig_foundations_quality) ||
            (this.wall_quality != orig_wall_quality)) {
            this.envChanged = Boolean.TRUE;
            updatePercepts();
        }
        return true;
    }
    
    public Map<String,Collection<Literal>> getPerceptMap() {
        
        Map<String, Collection<Literal>> result = new HashMap();
        for (String agentName: agentNames) {
            result.put(agentName, this.getPercepts(agentName));
        }
        return result;
    }
}