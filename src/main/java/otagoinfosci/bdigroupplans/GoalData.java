package otagoinfosci.bdigroupplans;

import com.google.gson.Gson;

public class GoalData {
    private final int numSubgoals;
    private final int joinTimeout;
    private final int completionTimeout;
    private boolean allJoined;
    private long allJoinedTime;
    private boolean failed;
    private boolean succeeded;
    
    public GoalData(int numSubgoals, int joinTimeout, int completionTimeout, 
                    boolean allJoined, boolean failed, boolean succeeded) {
        this.numSubgoals = numSubgoals;
        this.joinTimeout = joinTimeout;
        this.completionTimeout = completionTimeout;
        this.allJoined = allJoined;
        this.allJoinedTime = -1;
        this.failed = failed;
        this.succeeded = succeeded;
    }
    
    public int getNumSubgoals() {
        return numSubgoals;
    } 
    
    public int getJoinTimeout() {
        return joinTimeout;
    }    
    
    public int getCompletionTimeout() {
        return completionTimeout;
    }

    public boolean isAllJoined() {
        return allJoined;
    }

    public void setAllJoined(boolean allJoined) {
        this.allJoined = allJoined;
    }
    
    public long getAllJoinedTime() {
        return allJoinedTime;
    }

    public void setAllJoinedTime(long allJoinedTime) {
        this.allJoinedTime = allJoinedTime;
    }
    public boolean isFailed() {
        return failed;
    }

    public void setFailed(boolean failed) {
        this.failed = failed;
    }
    
    public boolean isSucceeded() {
        return succeeded;
    }

    public void setSucceeded(boolean succeeded) {
        this.succeeded = succeeded;
    }
    
    public String toString() {
        return String.format("GoalData(numSubgoals=%d,joinTimeout=%d,completionTimeout=%d," +
                             "allJoined=%b,allJoinedTime=%d, failed=%b, succeeded=%b)",
                             numSubgoals, joinTimeout, completionTimeout, allJoined, allJoinedTime, failed, isSucceeded());
    }
    
    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
    
    public static GoalData fromJson(String s) {
        Gson gson = new Gson();
        return gson.fromJson(s, GoalData.class);
    }

}