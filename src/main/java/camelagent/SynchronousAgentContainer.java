/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package camelagent;
import static camelagent.AgentContainer.agentList;
import camelagent.util.ContainerNamingStrategy;

/**
 *
 * @author SCranefield
 */
public class SynchronousAgentContainer extends AgentContainer {
    
    public SynchronousAgentContainer(ClassLoader cldr, Package pkg) {
	super(cldr, pkg);
    }
    
    public SynchronousAgentContainer(ContainerNamingStrategy strategy, ClassLoader cldr, Package pkg) {
        super(strategy, cldr, pkg);
    }
    
    @Override
    public void startAllAgents() {
        // One cycle only
        System.out.println("*** startAllAgents called ***");
        for (SimpleJasonAgent ag : agentList) {
            ag.getTS().reasoningCycle();
        }
    }
    
    public void cycleAgents() {
        System.out.println("*** cycleAgents called ***");
        for (SimpleJasonAgent ag : agentList) {
            ag.getTS().reasoningCycle();
        }
    }
}
