/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package camelagent.jason;

// Changed on 18/8/2020 due to changes in Jason:
// jason.runtime.RuntimeServicesInfraTier has become jason.runtime.RuntimeServices
// and interface jason.infra.InfrastructureFactory now returns a RuntimeServices object
// from method createRuntimeServices

import jason.infra.InfrastructureFactory;
import jason.infra.MASLauncherInfraTier;
//import jason.runtime.RuntimeServices;
import jason.runtime.RuntimeServicesInfraTier;

/**
 *
 * @author SCranefield
 */
public class CamelInfrastructureFactory implements InfrastructureFactory{

    @Override
            public MASLauncherInfraTier createMASLauncher() {
        throw new UnsupportedOperationException("Jason IDE not supported by Camel Infrastructure");
    }

    @Override
    public RuntimeServicesInfraTier createRuntimeServices() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
